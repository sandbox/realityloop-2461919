<?php

/**
 * @file
 * Core functions for File Field Sources - Attach from View module.
 */

/**
 * Implements hook_theme().
 */
function filefield_sources_attach_view_theme() {
  return array(
    'filefield_sources_attach_view_element' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_module_implements_alter().
 */
function filefield_sources_attach_view_module_implements_alter(&$implementations, $hook) {
  $hooks = array('form_alter', 'form_field_ui_field_edit_form_alter');

  if (in_array($hook, $hooks) && isset($implementations['filefield_sources_attach_view'])) {
    $implementation = $implementations['filefield_sources_attach_view'];
    unset($implementations['filefield_sources_attach_view']);
    $implementations['filefield_sources_attach_view'] = $implementation;
  }
}

/**
 * Implements hook_filefield_sources_info().
 */
function filefield_sources_attach_view_filefield_sources_info() {
  $sources['attach_view'] = array(
    'name'        => t('File attach from view'),
    'label'       => t('View'),
    'description' => t('Select a file from a View.'),
    'process'     => 'filefield_sources_attach_view_filefield_source_attach_view_process',
    'value'       => 'filefield_sources_attach_view_filefield_source_attach_view_value',
    'weight'      => 3,
  );

  return $sources;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function filefield_sources_attach_view_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  $instance = $form['#instance'];
  if (in_array($instance['widget']['type'], module_invoke_all('filefield_sources_widgets'))) {
    if (!empty($form['instance']['widget']['settings'])) {
      $settings = $instance['widget']['settings']['filefield_sources'];

      $form['instance']['widget']['settings']['filefield_sources']['source_attach_view'] = array(
        '#title'       => t('File attach from view settings'),
        '#type'        => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed'   => TRUE,
        '#description' => t('File attach from view allows for selecting a file provided by a view.'),
      );

      $options = array('' => '- Select -');
      $views   = views_get_all_views();
      foreach ($views as $view) {
        // @TODO - Test this works when display overrides master display.
        if ($view->base_table == 'file_managed' && !empty($view->display['default']->display_options['fields']) && isset($view->display['default']->display_options['fields']['uri'])) {
          foreach ($view->display as $display_id => $display) {
            $options[$view->name . ':' . $display_id] = $view->name . ' - ' . $view->display[$display_id]->display_title;
          }
        }
      }
      ksort($options);
      $default = !empty($settings['source_attach_view']['view']) ? $settings['source_attach_view']['view'] : NULL;

      $form['instance']['widget']['settings']['filefield_sources']['source_attach_view']['view'] = array(
        '#type'          => 'select',
        '#title'         => t('View'),
        '#options'       => $options,
        '#default_value' => $default,
        '#description'   => t('Only Views showing <em>Files</em> as <em>Fields</em> with the <em>File: Path</em> field are listed.'),
      );

      $form['instance']['widget']['settings']['filefield_sources']['source_attach_view']['args'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Arguments'),
        '#default_value' => !empty($settings['source_attach_view']['args']) ? $settings['source_attach_view']['args'] : NULL,
      );
    }
  }
}

/**
 * File Field Sources process callback for 'File attach from view' source.
 */
function filefield_sources_attach_view_filefield_source_attach_view_process($element, &$form_state, $form) {
  $instance = field_widget_instance($element, $form_state);
  $settings = $instance['widget']['settings']['filefield_sources']['source_attach_view'];

  $args = array($settings['args']);

  list($view, $display) = explode(':', $settings['view']);
  $view = views_get_view($view);
  $view->set_display($display);
  $view->set_arguments($args);
  $view->pre_execute();
  $view->execute();

  $element['filefield_sources_attach_view'] = array(
    '#weight'                      => 100.5,
    '#theme'                       => 'filefield_sources_attach_view_element',
    '#filefield_source'            => TRUE,
    '#filefield_sources_hint_text' => 'MOO',
  );

  $options = array();
  foreach ($view->result as $delta => $result) {
    // @TODO - Better targetting of this field alias.
    $options[$result->file_managed_uri]                  = '';
    $view->result[$delta]->filefield_sources_view_attach = TRUE;
  }

  $element['filefield_sources_attach_view']['uri'] = array(
    '#type'    => 'radios',
    '#options' => $options,
  );

  $element['filefield_sources_attach_view']['view'] = array(
    '#type'   => 'markup',
    '#markup' => $view->render(),
  );

  $element['filefield_sources_attach_view']['select'] = array(
    '#type'                    => 'submit',
    '#value'                   => t('Select'),
    '#validate'                => array(),
    '#submit'                  => array('filefield_sources_field_submit'),
    '#name'                    => $element['#name'] . '[filefield_sources_attach_view][button]',
    '#limit_validation_errors' => array($element['#parents']),
    '#ajax'                    => array(
      'path'    => 'file/ajax/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value'],
      'wrapper' => $element['#id'] . '-ajax-wrapper',
      'effect'  => 'fade',
    ),
  );

  return $element;
}

/**
 * File Field Sources value callback for 'File attach from view' source.
 */
function filefield_sources_attach_view_filefield_source_attach_view_value($element, &$item) {
  if (isset($item['filefield_sources_attach_view']) && isset($item['filefield_sources_attach_view']['uri'])) {
    $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);
    $uri      = $item['filefield_sources_attach_view']['uri'];

    // Check that the destination is writable.
    $directory = $element['#upload_location'];
    $mode      = variable_get('file_chmod_directory', 0775);

    // This first chmod check is for other systems such as S3, which don't work
    // with file_prepare_directory().
    if (!drupal_chmod($directory, $mode) && !file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      watchdog('file', 'File %file could not be copied, because the destination directory %destination is not configured correctly.', array(
        '%file'        => $uri,
        '%destination' => drupal_realpath($directory)
      ));
      drupal_set_message(t('The specified file %file could not be copied, because the destination directory is not properly configured. This may be caused by a problem with file or directory permissions. More information is available in the system log.', array('%file' => $uri)), 'error');

      return;
    }

    // Clean up the file name extensions and transliterate.
    $original_uri = $uri;
    $new_uri      = filefield_sources_clean_filename($uri, $instance['settings']['file_extensions']);
    rename($uri, $new_uri);
    $uri = $new_uri;

    // Run all the normal validations, minus file size restrictions.
    $validators = $element['#upload_validators'];
    if (isset($validators['file_validate_size'])) {
      unset($validators['file_validate_size']);
    }

    // Save the file to the new location.
    if ($file = filefield_sources_save_file($uri, $validators, $directory)) {
      $item = array_merge($item, (array) $file);
    }

    // Restore the original file name if the file still exists.
    if (file_exists($uri) && $uri != $original_uri) {
      rename($uri, $original_uri);
    }

    $item['filefield_sources_attach_view']['uri'] = '';
  }
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * Add placeholder field in View row for form element.
 */
function filefield_sources_attach_view_preprocess_views_view_fields(&$variables) {
  if (isset($variables['row']->filefield_sources_view_attach)) {
    $variables['fields']['filefield_sources_attach_view']                 = new stdClass();
    $variables['fields']['filefield_sources_attach_view']->wrapper_prefix = '';
    $variables['fields']['filefield_sources_attach_view']->label_html     = '';
    // @TODO - Better targetting of this field alias.
    $variables['fields']['filefield_sources_attach_view']->content        = "filefield_sources_view_attach-placeholder-{$variables['row']->file_managed_uri}";
    $variables['fields']['filefield_sources_attach_view']->wrapper_suffix = '';
  }
}

/**
 * @param $variables
 *
 * @return string
 */
function theme_filefield_sources_attach_view_element($variables) {
  $element = $variables['element'];

  // Replace View placeholder field with form element.
  foreach (element_children($element['uri']) as $uri) {
    $element['view']['#markup'] = str_replace("filefield_sources_view_attach-placeholder-{$uri}", drupal_render($element['uri'][$uri]), $element['view']['#markup']);
  }

  return '<div class="filefield-source filefield-source-attach_view clear-block">' . drupal_render_children($element) . '</div>';
}
